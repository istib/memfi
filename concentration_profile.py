from __future__ import division
import math
import matplotlib.pyplot as plt
import numpy as np


# Function to calculate and plot the concentration within a filter.
# In order to get the correct x and p values for this function,
# run "x, p, cdf, name = porosity_dist(L,n,function,p_0,p_end)"
# in the python file "Random porosity function".


def Determine_Q(L, P_diff, x, p):

    # L is the length of the filter.
    # This should be a user input, with values between 1 and 10.

    # P_diff is the pressure difference across the filter. This drives a fluid flux through the filter.
    # To obtain an order 1 flux, values of P_diff can vary between 50 and 1000. This is because of the
    # relationship Q = -K * dP/dx, and noting that K is roughly of the order 10^(-2).
    # This should be a user input, with values between 50 and 1000.

    # x is the vector with the gridpoints of the porosity p. Generated from "x, p, cdf, name = porosity_dist(L,n,function,p_0,p_end)"
    # in the python file "Random porosity function".

    # p is the vector with the values of the porosity. Generated from "x, p, cdf, name = porosity_dist(L,n,function,p_0,p_end)"
    # in the python file "Random porosity function".

    # dim is the dimension of the system we are considering. dim can be 2 or 3. Three-dimensions are more realistic, but two
    # dimensions are easier to view on a screen.

    # Ensure that the function inputs are the required type.
    L = float(L)
    P_diff = float(P_diff)

    dim = 2

    # These logic commands split between two and three dimensional obstacles. The data used is different, and the way
    # in which porosity is calculated from obstacle radius is also different.
    if dim == 2:
        # Load up external data.
        r_K, K_int = np.genfromtxt("data/Avu1availablespace_2D_flow_module.txt", unpack=True, skiprows=5)
        r_D, D_int = np.genfromtxt("data/dg1_by_dx_2D_avail_space.txt", unpack=True, skiprows=5)
        s_K = []
        porosity_K = []
        s_D = []
        porosity_D = []
        DD = []
        V = math.pi

        # Create porosity vector for the permeability data (K) for the possible radii of circles.
        for i in range(0, len(r_K)):
            s_K.append(math.pi*(r_K[i]**2))
            porosity_K.append(1 - s_K[i])

        # Create porosity vector for the diffusion data (D) for the possible radii of circles.
        for i in range(0, len(r_D)):
            s_D.append(math.pi*(r_D[i]**2))
            porosity_D.append(1 - s_D[i])
            DD.append(1 - D_int[i])

    elif dim == 3:
        # Load up external data.
        r_K, K_int = np.genfromtxt("data/Avu1availablespace_3D_flow_module.txt", unpack=True, skiprows=5)
        r_D, D_int = np.genfromtxt("data/dg1_by_dx_3D_avail_space.txt", unpack=True, skiprows=5)
        s_K = []
        porosity_K = []
        s_D = []
        porosity_D = []
        V = 4*math.pi/3.0
        DD = []

        # Create porosity vector for the permeability data (K) for the possible radii of spheres.
        for i in range(0, len(r_K)):
            s_K.append(4*math.pi*(r_K[i]**3)/3.0)
            porosity_K.append(1 - s_K[i])

        # Create porosity vector for the diffusion data (D) for the possible radii of spheres.
        for i in range(0, len(r_D)):
            s_D.append(4*math.pi*(r_D[i]**3)/3.0)
            porosity_D.append(1 - s_D[i])
            DD.append(1 - D_int[i])

    else:
        print 'Erroneous result - must use a dimension of 2 or 3'
        raise ValueError

    # Python writes K_int in a manner which can't be used to interpolate.
    # I therefore create a new permeability function K which is fine to interpolate.
    KK = []
    for item in K_int:
        KK.append(item)

    # The data is given for a decreasing porosity, and the interpolation function
    # in Python requires an increasing function to be used. We therefore reverse
    # the vectors.
    porosity_K.reverse()
    KK.reverse()
    porosity_D.reverse()
    DD.reverse()

    # Creating the diffusion functions.
    D = np.interp(p, porosity_D, DD)

    # The permeability function for a given porosity.
    K = np.interp(p, porosity_K, KK)

    # The inverse of the permeability function. This is integrated to obtain the average flux.
    K_inv = []
    for value in K:
        K_inv.append(1/value)

    # The flux of fluid through the filter.
    Q = P_diff/(np.trapz(K_inv, x))

    # This should be a visual output on the website.
    print 'The flux is', Q

    return Q, D, dim, V


def conc_profile(L, x, p, Q, k_coeff, D, dim, V, c_inf):

    # k_coeff is the uptake coefficient on the obstacle surface. This is experimentally determined.
    # This should be a user input, and vary between 0.1 and 10.

    # c_inf is the concentration far upstream of the filter.
    # This should be a user input, and vary between 0.1 and 10.

    dim = float(dim)
    k_coeff = float(k_coeff)

    # Determine grid spacing.
    h = x[1]-x[0]

    # The vectors used to create the matrix system representing the filtration problem.
    D_1 = [0.0]
    Dp_1 = [0.0]
    Qp_1 = []
    sink = []

    n = len(x)

    for k in range(0, n-1):
        D_1.append((D[k] + D[k+1])/2.0)  # (D_i + D_{i+1})/2
        Dp_1.append((D[k] + D[k+1])*(p[k+1] - p[k])/(2.0*(p[k] + p[k+1])))  # (D_i + D_{i+1})*(p_{i+1} - p_i)/(p_i + p_{i+1})
        Qp_1.append(Q/(2*p[k]))
        sink.append(dim*k_coeff*(math.pow(V, 1/dim))*(math.pow(1-p[k], 1-1/dim))/(p[k]))  # Sink coefficient

    Qp_1.append(Q/(2.0*p[n-1]))
    sink.append(dim*k_coeff*math.pow(V, 1/dim)*(math.pow(1-p[n-1], 1-(1/dim)))/(p[n-1]))
    D_1.append(0)
    Dp_1.append(0)

    Dfront = D_1[:-1]
    Dend = D_1[1:]

    Dpfront = Dp_1[:-1]
    Dpend = Dp_1[1:]

    Qp_1_front = Qp_1[:-1]
    Qp_1_end = Qp_1[1:]

    A0 = -np.diag(Dfront) - np.diag(Dend) + np.diag(Dpfront) - np.diag(Dpend) - (h**2)*np.diag(sink)

    A1 = np.diag(D_1[1:-1], 1) - np.diag(Dp_1[1:-1], 1) - h*np.diag(Qp_1_end, 1)

    A_1 = np.diag(D_1[1:-1], -1) + np.diag(Dp_1[1:-1], -1) + h*np.diag(Qp_1_front, -1)

    A = A0 + A1 + A_1

    # Impose boundary conditions
    A[0, :] = 0
    A[n-1, :] = 0

    # BC on left-hand side of filter.
    A[0, 0] = (2*h*Q + D[0]*(4*p[1] - p[2]))/(p[0])
    A[0, 1] = -4*D[0]
    A[0, 2] = 1*D[0]

    # Continuity of flux condition (on RHS of filter).
    A[-1, -1] = (4*p[-2] - p[-3])/(p[-1])
    A[-1, -2] = -4
    A[-1, -3] = 1

    # Right-hand side of the matrix equation.
    rhs = np.zeros(n)
    rhs[0] = 2*Q*h

    # Solve Ax = rhs
    c = np.linalg.solve(A, rhs)

    for k in range(0, len(c)):
        c[k] = c_inf*c[k]

    # fig = plt.figure(figsize=(20, 20/L))

    # ax = fig.add_subplot(111,
    #                      title="Concentration profile")

    # # Plot the concentration profile.
    # plt.plot(x, c, '-',  linewidth=2, c='k')
    # plt.title("Concentration profile")
    # plt.xlabel("x")
    # plt.ylabel("c", rotation='horizontal')

    return x, c


# # Test this function with some typical parameters.
# L = 10
# P_diff = 1000
# k_coeff = 1
# p_0 = 0.9
# p_L = 0.7
# c_inf = 1
# p = []
#
#
# # Define the grid points and porosity function. In general,  this task should be carried out by
# # the function "x, p, cdf, name = porosity_dist(L,n,function,p_0,p_end)" in the
# # python file "random_porosity.py".
# x = np.linspace(0, L, 100)
# for X in x:
#     p.append(p_0 + (p_L - p_0)*X/L)
#
# # Call the function which determines the flux, diffusion vector, and dimension.
# Q, D, dim, V = Determine_Q(L, P_diff, x, p)
#
#
# # Call the function which determines the concentration.
# c, c_end = conc_profile(L, x, p, Q, k_coeff, D, dim, V, c_inf)
