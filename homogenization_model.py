from __future__ import division
import math
import random
import matplotlib.pyplot as plt
import numpy as np
from bokeh.plotting import figure as bokeh_figure


def concave_model(x, phi_0, phi_end, L):
    "The concave function and its cumulative distribution function"

    b = math.log(phi_0 + 1 - phi_end)/L
    p = []
    cdf = []
    equation_image_filename = 'Concave.png'
    for X in x:
        p.append(phi_0 + 1 - math.exp(b*X))
        if phi_0 == phi_end:
            cdf.append(X/L)
        else:
            cdf.append(((1 + b*phi_0*X) - math.exp(b*X))/((1 + b*phi_0*L) - math.exp(b*L)))

    return p, cdf, equation_image_filename


def convex_model(x, phi_0, phi_end, L):
    "The convex function and its cumulative distribution function"

    b = math.log(phi_end / phi_0) / L
    p = []
    cdf = []
    equation_image_filename = 'Convex1.png'
    for X in x:
        p.append(phi_0*math.exp(b*X))
        if phi_0 == phi_end:
            cdf.append(X/L)
        else:
            cdf.append((phi_0*math.exp(b*X) - phi_0 - b*X)/(phi_0*math.exp(b*L) - phi_0 - b*L))

    return p, cdf, equation_image_filename


def linear_model(x, p_0, p_end, L):
    "The linear function and its cumulative distribution function"

    p = []
    cdf = []
    equation_image_filename = 'Linear.png'
    for X in x:
        p.append(p_0 + (p_end - p_0)*X/L)
        if p_0 == p_end:
            cdf.append(X/L)
        else:
            cdf.append((p_0*X + (p_end - p_0)*(X**2)/(2*L) - X)/(L*(p_end + p_0)/2 - L))

    return p, cdf, equation_image_filename


# A list of the potential function types available for the porosity distribution.
model_types = {
    'linear': linear_model,
    'concave': concave_model,
    'convex': convex_model
}


def porosity_dist(L, n, model_type, p_0, p_end):
    """
    Calculating the porosity distribution for given variables. Outputs the gridpoints, x,
    the porosity function, p, the cumulative distribution function, cdf, and the .png file
    of the function expression, name.

    L is the length of the filter.

    n is the number of gridpoints required to calculate the porosity function.

    model_type is a string which refers to the type of function required (as defined above).

    # p_0 and p_end are the start and end porosities.
    """

    x = np.linspace(0, L, n)
    p, cdf, name = model_types[model_type](x, p_0, p_end, L)

    return x, p, cdf, name


# def plot_porosity_dist(x, p):
#     "Plots the porosity distribution function."

#     plt.plot(x, p, '-',  linewidth=2, c='k')
#     plt.title("Porosity function")
#     plt.xlabel("x")
#     plt.ylabel("Porosity")

#     # Show or save plot.
#     plt.show()
#     #plt.savefig('Porosity_distribution_function.png')

#     return


def create_config(N_obstacles, L, obstacle_radius, p, maxattempts, x, cdf):

    # to store the (x,y) position of each obstacle
    conf = np.zeros((N_obstacles, 2))

    for i in range(0, N_obstacles):

        # so that it enters loop
        invalid_pos = 1
        # set attempts counter to zero
        attempts = 0
        while invalid_pos == 1 and attempts < maxattempts:
            C = random.random()
            I = np.interp(random.random(), cdf, x)
            conf[i, :] = [I, C]
            attempts = attempts + 1

            # haven't found any overlap so far.
            invalid_pos = 0
            for j in range(0, i):
                dist = math.sqrt((conf[i, 0]-conf[j, 0])**2 + (conf[i, 1]-conf[j, 1])**2)
                if (dist < 2*obstacle_radius):
                    invalid_pos = 1
                    # exit loop (no point checking for other overlaps)
                    break

        if invalid_pos == 1:
            print('maxattempts reached. Could not generate configuration.')

    return conf


def plot_circle(x, y, r):
    circle = plt.Circle((x, y), r, color='k')
    return circle


# def plot_obstacles(x, p, conf, L, eps):

#     for i in range(len(conf)):
#         fig = plt.gcf()
#         fig.gca().add_artist(plot_circle(conf[i, 0], conf[i, 1], eps))
#     plt.axis((0, L, 0, 1))
#     plt.show()
#     return


def plot_porosity_and_obstacles(model_type="linear",
                                phi_0=0.9,
                                phi_end=0.6,
                                N_obstacles=200,
                                L=10):

    # user inputs:
    # ===============
    # L       - should vary between 1 and 10.
    # phi_0   - should vary between 0.4 and 1.
    # phi_end - should vary between 0.4 and 1.

    # number of points used to evaluate the porosity function.
    n = 200

    # Maximum number of attempts for the randomly distributed spheres.
    max_attempts = 1000

    x, p, cdf, _ = porosity_dist(L,
                                 n,
                                 model_type,
                                 phi_0,
                                 phi_end)

    p_total = np.trapz(p, x)

    obstacle_radius = math.sqrt((L - p_total) / (N_obstacles * math.pi))

    configuration = create_config(N_obstacles,
                                  L,
                                  obstacle_radius,
                                  p,
                                  max_attempts,
                                  x,
                                  cdf)

    toolset = "crosshair,pan,reset,resize,save,wheel_zoom"

    # The size of the axis. Want it to be in the proportion x/y = L.
    plot = bokeh_figure(tools=toolset,
                        plot_width=400,
                        plot_height=400)

    plot.line(x, p)

    xs = map(lambda l: l[0], configuration)
    ys = map(lambda l: l[1], configuration)

    plot.scatter(xs, ys)

    # ax.spines['left'].set_color(color)
    # ax.yaxis.label.set_color(color)
    # ax.tick_params(axis='y',
    #                colors=color)

    # fig.add_axes((0,
    #               L,
    #               np.minimum(phi_0, phi_end),
    #               np.maximum(phi_0, phi_end)))

    # plt.xlabel("x")
    # plt.ylabel("Porosity function")

    # Plot the obstacles.

    # ax1 = ax.twinx()

    # for i in range(len(configuration)):
    #     f = plt.gcf()
    #     f.gca().add_artist(plot_circle(configuration[i, 0],
    #                                    configuration[i, 1],
    #                                    obstacle_radius))
    # ax1.set_ylim(0, 1)
    # plt.ylabel("y", rotation='horizontal')

    return plot
