import math
import matplotlib.pyplot as plt
import numpy as np
from scipy.sparse.linalg import spsolve


def conc_profile(L, Q, k_coeff, p_orig, beta):

    dim = 3.0

    L += 0.0
    Q += 0.0
    k_coeff += 0.0
    p_orig += 0.0
    beta += 0.0

    V = 4*math.pi/3

    n = 1000  # Number of gridpoints.

    x = np.linspace(0, L, n)  # Define meshpoints
    h = x[1]-x[0]

    p = []
    D = []

    for xi in x:
        pp = p_orig*math.exp(-beta*xi)
        p.append(pp)
        DD = (1.0 + pp)/2
        D.append(DD)

    D_1 = [0.0]
    Dp_1 = [0.0]
    Qp_1 = []
    sink = []
    for k in range(0, n-1):
        D_1.append((D[k] + D[k+1])/2)  # (D_i + D_{i+1})/2
        Dp_1.append((D[k] + D[k+1])*(p[k+1] - p[k])/(2*(p[k] + p[k+1])))  # (D_i + D_{i+1})*(p_{i+1} - p_i)/(p_i + p_{i+1})
        Qp_1.append(Q/(2*p[k]))
        sink.append(dim*k_coeff*math.pow(V, 1/dim)*(math.pow(1-p[k], 1-1/dim))/p[k])  # Sink coefficient

    Qp_1.append(Q/(2*p[n-1]))
    sink.append(dim*k_coeff*math.pow(V, 1/dim)*(math.pow(1-p[n-1], 1-1/dim))/p[n-1])  # Sink coefficient
    D_1.append(0.0)
    Dp_1.append(0.0)

    Dfront = D_1[:-1]
    Dend = D_1[1:]

    Dpfront = Dp_1[:-1]
    Dpend = Dp_1[1:]

    Qp_1_front = Qp_1[:-1]
    Qp_1_end = Qp_1[1:]

    A0 = -np.diag(Dfront) - np.diag(Dend) + np.diag(Dpfront) - np.diag(Dpend) - h**2*np.diag(sink)

    A1 = np.diag(D_1[1:-1], 1) - np.diag(Dp_1[1:-1], 1) - h*np.diag(Qp_1_end, 1)

    A_1 = np.diag(D_1[1:-1], -1) - np.diag(Dp_1[1:-1], -1) + h*np.diag(Qp_1_front, -1)

    A = A0 + A1 + A_1

    # Impose boundary conditions
    A[0, 0:] = 0
    A[n-1, 0:] = 0

    A[0, 0] = (2*h*Q + D[0]*(4*p[1] - p[2]))/p[0]
    A[0, 1] = -4*D[0]
    A[0, 2] = D[0]

    # Cont of flux condition.
    A[-1, -1] = (4*p[-2] - p[-3])/p[-1]
    A[-1, -2] = -4.0
    A[-1, -3] = 1.0

    #A = spdiags(bands, diags, n, n).tocsc()    # Form sparse matrix

    rhs = np.transpose([np.zeros(n)])
    rhs[0] = 2*Q*h

    # print rhs

    print A

    c = spsolve(A, rhs)  # Solve Ax = rhs

    plt.plot(x, c, '-',  linewidth=2)
    plt.title("Concentration profile")
    plt.xlabel("x")
    plt.ylabel("c", rotation='horizontal')
    plt.show()

    return


# conc_profile(10, 1, 1, 0.8, 0.01)
