import logging
logging.basicConfig(level=logging.DEBUG)

from bokeh.models import Plot, ColumnDataSource
from bokeh.properties import Instance
from bokeh.server.app import bokeh_app
from bokeh.server.utils.plugins import object_page
from bokeh.models.widgets import HBox, Slider, VBoxForm, Select, PreText, Button, Toggle, RadioGroup
from bokeh import mpl
from bokeh.plotting import figure, curdoc

from network_model_ui import NetworkModel
from homogenization_model_ui import HomogenizationModel

@bokeh_app.route("/bokeh/network_model/")
@object_page("network_model")
def make_network_model():
    m = NetworkModel.create()
    return m

@bokeh_app.route("/bokeh/homogenization_model/")
@object_page("homogenization_model")
def make_homogenization_model():
    m = HomogenizationModel.create()
    return m
