import math
import numpy as np
import xlrd


def Flux(c, k, Qs, V):
    """
    The function to calculate the flux, Q.


    c is the concentration of feed solution, and takes any value > 0.

    k is the adhesion parameter, and varies between 0 (no adhesion) and
    1 (complete adhesion). Typical values of around 0.1 - 0.2.

    Qs is the the proportion of flux that passes through a pore that is
    completely covered. Ranges from 0 (completely blocked) to 1
    (covering particles have no effect). Typical values are around
    0.1-0.2.

    V is the total throughput. This is the integral of Q with respect to time.
    """

    if c <= 0:
        raise Exception('The concentration of feed solution must be strictly greater than 0.')

    if k > 1 or k < 0:
        raise Exception('The adhesion parameter must be between 0 and 1.')

    if Qs > 1 or Qs < 0:
        raise Exception('The proportion of flux that passes through a blocked pore must be between 0 and 1.')

    if V < 0:
        raise Exception('The total throughput cannot be negative.')

    numerator = 1/(10*k) + math.exp(-1)

    denominator = 1/(10*k) + math.exp(math.pow(c*V, 2 - 2*Qs) - 1)

    Q = numerator/denominator

    return Q


def Max_throughput(c, k, Qs, Qmin):
    """
    The function to calculate the maximum throughput obtained.


    c is the concentration of feed solution, and takes any value > 0.

    k is the adhesion parameter, and varies between 0 (no adhesion) and
    1 (complete adhesion). Typical values of around 0.1 - 0.2.

    Qs is the the proportion of flux that passes through a pore that is
    completely covered. Ranges from 0 (completely blocked) to 1
    (covering particles have no effect). Typical values are around
    0.1-0.2.

    Qmin is the Flux at which the experiment is halted and the total throughput is obtained.
    """

    if c <= 0:
        raise Exception('The concentration of feed solution must be strictly greater than 0.')
    if k > 1 or k < 0:
        raise Exception('The adhesion parameter must be between 0 and 1.')
    if Qs > 1 or Qs < 0:
        raise Exception('The proportion of flux that passes through a blocked pore must be between 0 and 1.')
    if Qs == 1:
        raise Exception('As the proportion of flux that passes through a blocked pore is 1, there is no maximum flux.')
    if Qmin < 0:
        raise Exception('The flux at which the experiment is halted cannot be negative.')

    inside = 1 + math.log((1/(10*k) + math.exp(-1))/Qmin - 1/(10*k))
    V = math.pow(inside, 1/(2 - 2*Qs))/c

    return V


def plotQV(c, k, Qs, Qmin):
    """
    The function to plot the Q-V profile
    """

    n = 100
    V = np.linspace(0, Max_throughput(c, k, Qs, Qmin), n)

    Q_range = []

    for v in V:
        Q_range.append(Flux(c, k, Qs, v))

    # plt.plot(V, Q_range, '-',  linewidth=2)
    # plt.title("Q-V profile")
    # plt.xlabel("V")
    # plt.ylabel("Q", rotation='horizontal')
    # plt.show()

    return V, Q_range

# A sample Q-V profile.
# plotQV(1, 0.1, 0.1, 0.01)


def get_experimental_data():
    workbook = xlrd.open_workbook('data/Modified_data_file.xls')
    worksheet = workbook.sheet_by_name('Sheet1')
    num_rows = worksheet.nrows - 1
    # num_cells = worksheet.ncols - 1
    curr_row = -1

    Q = []
    V = []

    while curr_row < num_rows:
        curr_row += 1
        Q.append(worksheet.cell_value(curr_row, 1))
        V.append(worksheet.cell_value(curr_row, 0)/worksheet.cell_value(num_rows, 0))

    # plt.scatter(V, Q, c='k')
    # plt.axis([0, 1.1, 0, 1.1])
    # plt.title("Q-V curve from experiments")
    # plt.xlabel("V")
    # plt.ylabel("Q", rotation='horizontal')
    # plt.show()

    return Q, V
