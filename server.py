import sys

from flask import (Flask,
                   request,
                   session,
                   redirect, url_for,
                   render_template)


#---------------------
# to run matplotlib on server instance
import matplotlib
matplotlib.use('Agg')
#---------------------

app = Flask(__name__,
            template_folder="templates")

app.config['USERNAME'] = "griffiths"
app.config['PASSWORD'] = "mrbiscuits"
app.config['CSRF_ENABLED'] = True
app.config['SECRET_KEY'] = 'A0Zr98j/!yX9R~X%H!jmN2LW!/,?RT'


print """
                            __ _
                           / _(_)
  _ __ ___   ___ _ __ ___ | |_ _
 | '_ ` _ \ / _ \ '_ ` _ \|  _| |
 | | | | | |  __/ | | | | | | | |
 |_| |_| |_|\___|_| |_| |_|_| |_|
"""

# environment is defined from the command line (!)
if len(sys.argv) > 1 and str(sys.argv[1]) is "production":
    app.config['PRODUCTION'] = True
    print "..... running in PRODUCTION mode"
else:
    # local

    print "..... running in LOCAL mode"
    app.config['PRODUCTION'] = False

print ""
print ""
print ""

@app.route('/login/', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            return redirect(url_for('index'))
    return render_template('login.html', error=error)


@app.route("/")
def index():
    if app.config['PRODUCTION']:
        bokeh_server_prefix = ""
    else:
        bokeh_server_prefix = "http://localhost:5006"

    return render_template("index.html", server_prefix=bokeh_server_prefix)

if __name__ == "__main__":
    app.run(debug=True)
