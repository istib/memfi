from bokeh.models import Plot, ColumnDataSource
from bokeh.properties import Instance
from bokeh.models.widgets import HBox, VBox, Slider, VBoxForm
from bokeh.plotting import figure

from homogenization_model import porosity_dist, plot_porosity_and_obstacles
from concentration_profile import Determine_Q, conc_profile

TOOLSET = "pan,reset,save,wheel_zoom"


class HomogenizationModel(VBox):
    extra_generated_classes = [["HomogenizationModel", "HomogenizationModel", "VBox"]]

    membrane_section = Instance(HBox)
    model_section = Instance(HBox)

    membrane_inputs = Instance(VBoxForm)
    model_inputs = Instance(VBoxForm)

    phi_0 = Instance(Slider)
    phi_end = Instance(Slider)
    length = Instance(Slider)

    P_diff = Instance(Slider)
    k_coeff = Instance(Slider)
    c_0 = Instance(Slider)

    membrane_figure = Instance(Plot)
    membrane_figure_source = Instance(ColumnDataSource)

    model_plot = Instance(Plot)
    model_plot_source = Instance(ColumnDataSource)

    @classmethod
    def create(cls):
        obj = cls()

        obj.membrane_section = HBox()
        obj.model_section = HBox()

        obj.phi_0 = Slider(
            title="Initial porosity (phi_0)",
            name='initial_porosity',
            value=0.2,
            start=0.4,
            end=0.99)

        obj.phi_end = Slider(
            title="Final porosity (phi_end)",
            name='final_porosity',
            value=0.8,
            start=0.4,
            end=0.99)

        obj.length = Slider(
            title="Length of filter (L)",
            name='filter_length',
            value=5,
            start=1,
            end=10)

        obj.P_diff = Slider(
            title="Pressure difference (P_diff)",
            name='pressure_difference',
            value=200,
            start=50,
            end=1000)

        obj.k_coeff = Slider(
            title="Uptake coefficient (k)",
            name='uptake_coefficient',
            value=1,
            start=0.1,
            end=10)

        obj.c_0 = Slider(
            title="Concentration (c_0)",
            name='concentration',
            value=10,
            start=1,
            end=10)

        obj.membrane_inputs = VBoxForm(
            children=[
                obj.phi_0,
                obj.phi_end,
                obj.length,
                ])

        obj.model_inputs = VBoxForm(
            children=[
                obj.P_diff,
                obj.k_coeff,
                obj.c_0,
                ])

        obj.membrane_figure_source = ColumnDataSource(data=dict(left_x=[],
                                                                left_y=[],
                                                                right_x=[],
                                                                right_y=[]))

        obj.membrane_figure = figure(plot_height=400,
                                     plot_width=400,
                                     tools="",
                                     x_range=[-0.1, 1.1],
                                     y_range=[-0.1, 1.1],
                                     title="Membrane shape")

        obj.membrane_figure.line('left_x', 'left_y',
                                 source=obj.membrane_figure_source,
                                 line_width=3,
                                 line_alpha=1)
        obj.membrane_figure.line('right_x', 'right_y',
                                 source=obj.membrane_figure_source,
                                 line_width=3,
                                 line_alpha=1)

        obj.membrane_figure.axis.major_label_text_font_size = "0"
        obj.membrane_figure.xaxis.major_tick_out = 0


        obj.model_plot_source = ColumnDataSource(data=dict(x=[], y=[]))

        obj.model_plot = figure(plot_height=400,
                                plot_width=400,
                                tools="",
                                # x_range=[-0.1, 1.1],
                                # y_range=[-0.1, 1.1],
                                title="Concentration function")

        obj.model_plot.line('x', 'y',
                            source=obj.model_plot_source,
                            line_width=3,
                            line_alpha=1)

        obj.update_plots()

        obj.membrane_section.children.append(obj.membrane_inputs)
        obj.membrane_section.children.append(obj.membrane_figure)

        obj.model_section.children.append(obj.model_inputs)
        obj.model_section.children.append(obj.model_plot)

        obj.children.append(obj.membrane_section)
        obj.children.append(obj.model_section)

        return obj

    def setup_events(self):
        """Registers events each time the app changes state."""

        super(HomogenizationModel, self).setup_events()

        for w in ["phi_0",
                  "phi_end",
                  "length",
                  ]:
            input_field = getattr(self, w)
            if input_field:
                input_field.on_change('value', self, 'input_change')

        for w in ["P_diff",
                  "k_coeff",
                  "c_0"]:
            input_field = getattr(self, w)
            if input_field:
                input_field.on_change('value', self, 'input_change')

    def input_change(self, obj, attrname, old, new):
        self.update_plots()

    def update_plots(self):

        phi_0 = self.phi_0.value
        phi_end = self.phi_end.value
        # length = self.length.value

        top_left_x = 0.5 - (phi_0 /2)
        top_left_y = 1
        top_right_x = 0.5 + (phi_0 /2)
        top_right_y = 1

        bottom_left_x = 0.5 - (phi_end /2)
        bottom_left_y = 0.1
        bottom_right_x = 0.5 + (phi_end /2)
        bottom_right_y = 0.1

        left_x, left_y = [top_left_x, bottom_left_x], [top_left_y, bottom_left_y]
        right_x, right_y = [top_right_x, bottom_right_x], [top_right_y, bottom_right_y]

        self.membrane_figure_source.data = dict(left_x=left_x,
                                                left_y=left_y,
                                                right_x=right_x,
                                                right_y=right_y)

        P_diff = self.P_diff.value
        k_coeff = self.k_coeff.value
        c_0 = self.c_0.value

        model_type = "linear"
        phi_0 = self.phi_0.value
        phi_end = self.phi_end.value
        L = 10
        n = 200  # ??

        x, p, cdf, name = porosity_dist(L,
                                        n,
                                        model_type,
                                        phi_0,
                                        phi_end)

        Q, D, dim, V = Determine_Q(L, P_diff, x, p)

        x, c = conc_profile(L, x, p, Q, k_coeff, D, dim, V, c_0)

        # final concentration (TODO: display on UI)
        c_end = c[-1]

        self.model_plot_source.data = dict(x=x,
                                           y=c)
