# Memfi: Modelling and Experiments: A Membrane Filtration Interface

![Memfi logo](/static/img/memfi-logo.jpg)

Website: [http://www.memfi.co.uk](http://www.memfi.co.uk)

Copyright 2015. Dr. Ian Griffiths, Dr. Maria Bruna, Dr. Mohit Dalwadi

Oxford University. Department of Mathematics & Department of Computer Science



# Installing locally

1. Download and install the Anaconda python distribution, by following the instructions here:

    [http://continuum.io/downloads](http://continuum.io/downloads)


