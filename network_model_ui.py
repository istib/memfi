from bokeh.models import Plot, ColumnDataSource
from bokeh.properties import Instance
from bokeh.models.widgets import HBox, Slider, VBoxForm, Select, PreText, Button, Toggle, RadioGroup
from bokeh.plotting import figure

from network_model import plotQV, get_experimental_data

TOOLSET = "pan,reset,save,wheel_zoom"


def downsample(x, y):
    #
    # SHOULDN'T BE NECESSARY, but plot.scatter seems to have an upper bound of data points it can show??!!?
    out_x = []
    out_y = []
    for i in range(0, len(x)):
        if (i % 2) == 1:
            out_x.append(x[i])
            out_y.append(y[i])

    return out_x, out_y


class NetworkModel(HBox):
    extra_generated_classes = [["NetworkModel", "NetworkModel", "HBox"]]

    inputs = Instance(VBoxForm)

    c = Instance(Slider)
    k = Instance(Slider)
    Qs = Instance(Slider)
    Qmin = Instance(Slider)

    show_experimental_data = Instance(RadioGroup)

    upload_prompt = Instance(Button)
    fit_prompt = Instance(Button)

    fit = Instance(PreText)

    plot = Instance(Plot)
    source = Instance(ColumnDataSource)


    @classmethod
    def create(cls):
        obj = cls()

        obj.c = Slider(title="Concentration of Solution (c)",
                       name='c',
                       value=0.9,
                       start=0.01,
                       end=1)

        obj.k = Slider(title="Adhesivity (k)",
                       name='k',
                       value=0.1,
                       start=0.01,
                       end=1)

        obj.Qs = Slider(title="Leakage Flux (Q*)",
                        name='Qs',
                        value=0.2,
                        start=0.01,
                        end=1)

        obj.Qmin = Slider(title="Flux at total throughput (Qmin)",
                          name='Qmin',
                          value=0.5,
                          start=0.01,
                          end=1)

        obj.upload_prompt = Button(label="Upload Data",
                                   type="primary")

        obj.fit_prompt = Button(label="Fit Data",
                                type="success")

        obj.show_experimental_data = RadioGroup(labels=["show experimental data", "hide"],
                                                active=0)

        obj.fit = PreText(text="Fit", width=200)

        obj.inputs = VBoxForm(
            children=[
                obj.c,
                obj.k,
                obj.Qs,
                obj.Qmin,
                obj.show_experimental_data,
                # obj.upload_prompt,
                # obj.fit_prompt,
                ])

        obj.source = ColumnDataSource(data=dict(x=[],
                                                y=[],
                                                exp_x=[],
                                                exp_y=[]))

        obj.plot = figure(plot_height=400,
                          plot_width=400,
                          tools=TOOLSET,
                          title="QV Signature")

        obj.plot.line('x', 'y',
                      source=obj.source,
                      line_width=3,
                      line_alpha=0.6)

        obj.plot.scatter('exp_x', 'exp_y',
                         source=obj.source,
                         color="red")

        obj.update_plot()

        obj.children.append(obj.inputs)
        obj.children.append(obj.plot)

        return obj

    def fit_data(self):
        print "####################"
        self.c.value = 1

    def setup_events(self):
        """Registers events each time the app changes state."""

        super(NetworkModel, self).setup_events()

        for w in ["c",
                  "k",
                  "Qs",
                  "Qmin",
                  ]:
            input_field = getattr(self, w)
            if input_field:
                input_field.on_change('value', self, 'input_change')

        if self.show_experimental_data:
            self.show_experimental_data.on_change('active', self, 'input_change')

        if self.fit_prompt:
            self.fit_prompt.on_click(self.fit_data)

    def input_change(self, obj, attrname, old, new):
        self.update_plot()

    def update_plot(self):

        c = self.c.value
        k = self.k.value
        Qs = self.Qs.value
        Qmin = self.Qmin.value

        show_data = self.show_experimental_data.active

        x, y = plotQV(c,
                      k,
                      Qs,
                      Qmin)

        if show_data == 0:
            exp_x, exp_y = get_experimental_data()
            exp_x, exp_y = downsample(exp_x, exp_y)
        else:
            exp_x, exp_y = [], []

        self.source.data = dict(x=x,
                                y=y,
                                exp_x=exp_x,
                                exp_y=exp_y)
